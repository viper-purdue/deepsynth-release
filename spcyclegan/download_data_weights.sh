# download example datasets
wget https://skynet.ecn.purdue.edu/~micro/deepsynth-distribute/deepsynth_release/deepsynth_data/spcyclegan_data/datasets.zip --no-check-certificate
unzip datasets.zip
# download trained weights
wget https://skynet.ecn.purdue.edu/~micro/deepsynth-distribute/deepsynth_release/deepsynth_data/spcyclegan_data/checkpoints.zip --no-check-certificate
unzip checkpoints.zip
# download example results
wget https://skynet.ecn.purdue.edu/~micro/deepsynth-distribute/deepsynth_release/deepsynth_data/spcyclegan_data/results.zip --no-check-certificate
unzip results.zip
